# Performance problems with dotnet 5 and Oracle's 'MySql.Data' Version="8.0.22"? 

So you have decided to start your new dotnet project with a MySQL database? And find the performance unexcpectedly poor? 

The 'default' connection from Oracle, the makers of MySQL, appears to be a resonable choice. The driver however does not perform as expected, and your newly setup MySQL server is significantly slower your MsSQL server, by a magnitude.

The problem chosing the 'wrong' MySQL driver for dotnet

To compare MySQL vs and MsSQL, the two database engines were setup as docker container, on a single server, and loaded with identical tables.

```SQL
-- table holds 13500 rows
SELECT ae.* from AccountEntry ae where ae.companyID = 1030 -- Query 1, yields 1261 rows
SELECT ae.* from AccountEntry ae where ae.companyID = 1037 -- query 2, yields 4684 rows
```

The issue was MySQL execution time was slow, and should not take 1/2 seconds to return 1200-4000 rows for a single table query.

### Test 1 - dotnet, Dapper and MySql vs Ms-SQL Server
- 638ms (MySQL) vs 20ms (MsSql) for 4684 rows as per above query.

The results are using 'getting started' examples for Oracle's provided driver. Oracle's drivers are surprisingly slow.

### Test 2 - getting rid of Dapper 
Getting rid of Dapper for the MySQL code did nothing much.
     - 543ms (MySQL) vs 20ms (MsSql)
    
Dapper was certainly slowing matters significantly down, but not enough to explain the difference.

And the performance was still awful! So perhaps this was MySQL setup issue? (its was not) 

Lets test the performance from Go code.

### Test 3 - testing MySQL database service from Go code. 
    - 26ms (MySQL/Go) vs 20ms (MsSql/dotnet)

Thats great. So my MySQL database is fine. 

However Go and MySQL appeared not to be excactly best frinds, and parsing 'bool' needed various work-arounds. This was anyway a c# project, so GO is really not an option. (GO vs MySQL: https://github.com/go-sql-driver/mysql/issues/440 ) 

### Test 4 - changing the MySQL driver
Changing the driver from Oracle's MySQL.data driver to MyConnector driver solved the performance issue, immediately, with or without Dapper.
 
   - 32ms (MySQL/dotnet) vs 20ms (MsSql/dotnet)

## Conclusion
Do not use Oracle's driver (MySQL.Data) and instead use **MySqlConnector**.

``` sh
dotnet add package MySqlConnector # --version 1.2.1
```

You can find some performance graphs from the authors of MySQLConnector

https://mysqlconnector.net/

I am getting getting even better performance than this.

## Test Results 

| DB    | Connector            | ORM        | Language |  query1 I | query2 II| 
| :-----| :------              |:----------:|----------:|  -------:| ---------:| 
| MySQL | MySql.Data           | Dapper     | dotnet 5.0 |   206ms | 638ms |
| MySQL | MySql.Data           | *n/a       | dotnet 5.0 |   161ms | 543ms | 
| MySQL | MyConnector          | Dapper     | dotnet 5.0 |    14ms |  32ms |
| MySQL | MyConnector          | *n/a       | dotnet 5.0 |     7ms |  18ms | 
| MySQL | go-sql-driver/mysql  | n/a        | GO 1.15.6  |    16ms |  26ms |
| MsSQL | SqlClient            | Dapper     | dotnet 5.0 |    13ms |  20ms |
\* no objects or collections are created.
\*\* Warmup queries are executed prior running
 
## Hardware
### Client
- iMac late 2012 
- 3,4 GHz Quad-Core Intel Core i7
- 24 GB 1600 MHz DDR3
- MacOS 10.15.7 

### Server
- Intel NUC Intel Core i5
- Ubuntu LTS 20.04.01   
- MySQL and MsSQL are running within docker containers, all on the same server  

### Dotnet
| Framework                           | Connector                                         |
| :----------------------------| :---------------------------------------------------     |
| dotnet 5.0.100               |                                                          |
| Dapper v2.0.78               |   https://www.nuget.org/packages/Dapper/                 |
| MySql.Data v8.0.22           |   https://www.nuget.org/packages/MySql.Data/8.0.22       |
| System.Data.SqlClient v4.8.2 |   https://www.nuget.org/packages/System.Data.SqlClient   |
| MySqlConnector v1.2.1        |   https://www.nuget.org/packages/MySqlConnector/         |

### Go
| Language                     | Connector                                                |
| :----------------------------| :---------------------------------------------------     |
|  go 1.15.6                   | github.com/go-sql-driver/mysql 


## Messages sent and received to the developers of the Mysql.Data
```
message to the owners of the package                          Date: 2021-01-06
https://www.nuget.org/packages/MySql.Data/8.0.22

I am having performance issues with the MySQL and dotnet 5.0

On a simple query dotnet with 
- MsSQL gives an execution time of 20ms 
- MySQL gives an execution time of 515ms 

I am using Dapper, however Dapper is not the problem, I have similar execution time to MySQL with or without Dapper.

My MySQL instances appears to be in order, and I can execute the same query with GO in 26ms to MySQL (or similar of the response time I get with MsSQL and dotnet 5.0)

My tests, readme and code is found here.

https://bitbucket.org/LarsKlingsten/dotnet5-mysql-performance-problems/src/master/

```

```
From: Oracle Support <@oracle.com>   To: Lars Klingsten         Date: 2021-01-11

Hello Lars,

We are working in the issue you reported, we want to reproduce the same 
scenario as you using also your database model, I wonder if you can 
share your database script (SQL) to create the same tables from our side?,
```

```
From: Lars Klingsten         To: Oracle Support <@oracle.com>   Date: 2021-01-11

Hej (again)

I have uploaded the SQL Create table, and insert statements, to the repository. Text fields and amounts have been randomized, however are the exactly the same length as the original. 

I have removed a bit of unnecessary code (parsing command lines arguments). 

My Results are unchanged as follows (discarding the warmup)

rows=1261        exe=151ms        err=''         type=MySQL_Data 
rows=4686        exe=575ms        err=''         type=MySQL_Data 
rows=1261        exe=155ms        err=''         type=MySQL_Data_NoDapper 
rows=4686        exe=600ms        err=''         type=MySQL_Data_NoDapper 
rows=1261        exe=14ms         err=''         type=MySql_MyConnector_NoDapper 
rows=4686        exe=20ms         err=''         type=MySql_MyConnector_NoDapper 
rows=1261        exe=10ms         err=''         type=MySql_MyConnector 
rows=4686        exe=23ms         err=''         type=MySql_MyConnector 
rows=1261        exe=168ms        err=''         type=MySQL_Data             <-- re-run
rows=4686        exe=545ms        err=''         type=MySQL_Data             <-- re-run

rgds
```

``` 
UPDATE  2021-01-21

The performance issue has been known since 2020-11-23 and was registered as

Bug #101714 	Critical performance issue
Submitted:  	23 Nov 2020 6:59

https://bugs.mysql.com/bug.php?id=101714

and is still unresolved as today.

```

```
To: Oracle Support <@oracle.com>   From: Lars Klingsten         Date: 2021-01-22

Hi Oracle Support,

It appears that the performance issues that I reported on 2021-01-06 are duplicate of https://bugs.mysql.com/bug.php?id=101714

This bug was reported on already 2020-11-23 now two months ago.

In all fairness I believe that you should pull your own driver, and instead recommend developers the MySqlConnector Version="1.2.1", until the issues with your own driver has been resolved.

Kind regards / Lars Klingsten
```