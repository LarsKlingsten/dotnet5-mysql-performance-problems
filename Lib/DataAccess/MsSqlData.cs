namespace AccountSys3.Lib.DataAccess {

    using System;
    using System.Data.SqlClient;
    using Dapper;
    using AccountSys3.Lib.Models;
    using AccountSys3.Lib.Interfaces;
    using System.Diagnostics;
    using System.Collections.Generic;
    using AccountSys3.Lib.Enums;

    public class MsSqlData : IDataAccess {

        private string connStr = null;

        public MsSqlData(string connStr) {
            this.connStr = connStr;
        }

        public DatabaseTypes DatabaseType() {
            return DatabaseTypes.MsSQL_SqlClient;
        }

        public DbResp<T> Insert<T>(string insertStmt, T obj) {

            var sw = Stopwatch.StartNew();
            var dbResp = new DbResp<T>();

            try {
                using (var conn = new SqlConnection(connStr)) {
                    dbResp.RowsEffected = conn.Execute(insertStmt, obj);
                }
            } catch (SqlException e) { dbResp.Error = e.Message; } catch (Exception e) { dbResp.Error = e.Message; }

            dbResp.ExeTimeMS = sw.ElapsedMilliseconds;

            return dbResp;
        }

        public DbResp<T> Select<T>(string sql) {

            var sw = Stopwatch.StartNew();
            var dbResp = new DbResp<T>();

            try {
                using (var conn = new SqlConnection(connStr)) {
                    IList<T> result = conn.Query<T>(sql).AsList();
                    dbResp.RowsEffected = result.Count;
                    dbResp.Data = result;
                }
            } catch (SqlException e) { dbResp.Error = e.Message; } catch (Exception e) { dbResp.Error = e.Message; }

            dbResp.ExeTimeMS = sw.ElapsedMilliseconds;

            return dbResp;
        }


    }

}