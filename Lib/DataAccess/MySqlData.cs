namespace AccountSys3.Lib.DataAccess {

    using System;
    using MySql.Data.MySqlClient;
    using Dapper;
    using AccountSys3.Lib.Models;
    using AccountSys3.Lib.Interfaces;
    using System.Diagnostics;
    using System.Collections.Generic;
    using AccountSys3.Lib.Enums;

    public class MySqlData : IDataAccess {

        private string connectionString = null;

        public MySqlData(string connStr) {
            connectionString = connStr;
        }

        public DatabaseTypes DatabaseType() {
            return DatabaseTypes.MySQL_Data;
        }

        public DbResp<T> Insert<T>(string insertStmt, T obj) {

            var sw = Stopwatch.StartNew();
            var dbResp = new DbResp<T>();

            try {
                using (var conn = new MySqlConnection(connectionString)) {
                    dbResp.RowsEffected = conn.Execute(insertStmt, obj);
                }
            } catch (MySqlException e) { dbResp.Error = e.Message; } catch (Exception e) { dbResp.Error = e.Message; }

            dbResp.ExeTimeMS = sw.ElapsedMilliseconds;
            return dbResp;
        }

        public DbResp<T> Select<T>(string sql) {

            var sw = Stopwatch.StartNew();
            var dbResp = new DbResp<T>();

            try {
                using (var conn = new MySqlConnection(connectionString)) {
                    IList<T> result = conn.Query<T>(sql).AsList();
                    dbResp.RowsEffected = result.Count;
                    dbResp.Data = result;
                }
            } catch (MySqlException e) { dbResp.Error = e.Message; } catch (Exception e) { dbResp.Error = e.Message; }

            dbResp.ExeTimeMS = sw.ElapsedMilliseconds;

            return dbResp;
        }


    }

}