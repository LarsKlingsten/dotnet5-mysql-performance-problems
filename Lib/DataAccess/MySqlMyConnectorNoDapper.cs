namespace AccountSys3.Lib.DataAccess {

    using System;
    using MySqlConnector;
    using AccountSys3.Lib.Models;
    using AccountSys3.Lib.Interfaces;
    using System.Diagnostics;
    using System.Collections.Generic;
    using AccountSys3.Lib.Enums;

    public class MySqlMyConnectorNoDapper : IDataAccess {

        private string connectionString = null;

        public MySqlMyConnectorNoDapper(string connStr) {
            connectionString = connStr;
        }

        public DatabaseTypes DatabaseType() {
            return DatabaseTypes.MySql_MyConnector_NoDapper;
        }

        public DbResp<T> Insert<T>(string insertStmt, T obj) {
            throw new Exception("Not implemented. ");
        }

        public DbResp<T> Select<T>(string sql) {

            var sw = Stopwatch.StartNew();
            var dbResp = new DbResp<T>();

            try {
                using (var conn = new MySqlConnection(connectionString)) {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    var count = 0;
                    while (rdr.Read()) {
                        // Console.WriteLine(rdr[0] + " -- " + rdr[1]);
                        count++;
                    }
                    rdr.Close();

                    dbResp.RowsEffected = count;
                    dbResp.Data = null;
                }
            } catch (MySqlException e) { dbResp.Error = e.Message; } catch (Exception e) { dbResp.Error = e.Message; }

            dbResp.ExeTimeMS = sw.ElapsedMilliseconds;

            return dbResp;
        }


    }

}