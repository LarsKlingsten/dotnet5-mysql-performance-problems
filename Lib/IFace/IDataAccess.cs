namespace AccountSys3.Lib.Interfaces {
    using AccountSys3.Lib.Enums;
    using AccountSys3.Lib.Models;

    public interface IDataAccess {
        DatabaseTypes DatabaseType();
        DbResp<T> Insert<T>(string sql, T obj);
        DbResp<T> Select<T>(string sql);

    }
}