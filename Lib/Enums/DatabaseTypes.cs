namespace AccountSys3.Lib.Enums {

    public enum DatabaseTypes {
        MsSQL_SqlClient,
        MySQL_Data,
        MySQL_Data_NoDapper,
        MySql_MyConnector,
        MySql_MyConnector_NoDapper
    }

}