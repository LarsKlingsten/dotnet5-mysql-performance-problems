namespace AccountSys3.Lib.Service {
    using System;
    using System.IO;
    using AccountSys3.Lib.Enums;
    using System.Collections.Generic;

    public class SqlConnectionStringService {

        private string _filePath;
        private string _connectionString;

        private static readonly Dictionary<DatabaseTypes, string> PATH_DICT = new Dictionary<DatabaseTypes, string> {
              { DatabaseTypes.MsSQL_SqlClient, @"../../passwords-secrets/db/mssql-nuc-docker.txt"},
              { DatabaseTypes.MySQL_Data, @"../../passwords-secrets/db/mysql-nuc-docker.txt"},
              { DatabaseTypes.MySQL_Data_NoDapper, @"../../passwords-secrets/db/mysql-nuc-docker.txt"},
              { DatabaseTypes.MySql_MyConnector_NoDapper, @"../../passwords-secrets/db/mysql-nuc-docker.txt"},
              { DatabaseTypes.MySql_MyConnector, @"../../passwords-secrets/db/mysql-nuc-docker.txt"},
           
            //    { DatabaseTypes.MsSQL, @"../../../passwords-secrets/db/mssql-nuc-docker.txt"},
            //   { DatabaseTypes.MySQL, @"../../../passwords-secrets/db/mysql-nuc-docker.txt"},
            //   { DatabaseTypes.MySQLNoDapper, @"../../../passwords-secrets/db/mysql-nuc-docker.txt"},
          
            };

        public SqlConnectionStringService(DatabaseTypes type) {
            this._filePath = PATH_DICT.GetValueOrDefault(type);

            if (_filePath == null) {
                throw new Exception($"No implementation for database type '{type}'.");
            }
        }

        ///<summary>hint: check 'error' for non null value </summary>
        public (string connStr, string error) GetConnectionString() {

            string error = null;

            if (_connectionString == null) {
                try {
                    _connectionString = File.ReadAllText(_filePath, System.Text.Encoding.UTF8);

                } catch (Exception ex) {
                    error = ex.Message.ToString();
                }
            }

            return (this._connectionString, error);
        }
    }
}
