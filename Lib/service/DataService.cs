namespace AccountSys3.Lib.Service {
    using System;
    using AccountSys3.Lib.Enums;
    using AccountSys3.Lib.Interfaces;
    using AccountSys3.Lib.Models;

    public class DataService {

        private IDataAccess dataAccess;

        public DataService(IDataAccess dataAccess) {
            this.dataAccess = dataAccess;
        }

        public DatabaseTypes DatabaseType() {
            return dataAccess.DatabaseType();
        }

        public Models.DbResp<string> TestUnitTest() {
            return new Models.DbResp<string> { Error = "not implemented" };
        }

        public Models.DbResp<AccountEntry> GetAccountEntries(int companyID) {
            var sql = @$"SELECT ae.* from AccountEntryPerformanceNoFK ae where ae.companyID = {companyID}";
            var accountEntries = this.dataAccess.Select<AccountEntry>(sql);
            printAccountEntries(accountEntries);
            return accountEntries;
        }

        private void printAccountEntries(Models.DbResp<AccountEntry> accountEntries) {
            Console.WriteLine($"rows={accountEntries.RowsEffected} \t exe={accountEntries.ExeTimeMS}ms \t  err='{accountEntries.Error}' \t type={dataAccess.DatabaseType()} \t");
        }


    }

}



