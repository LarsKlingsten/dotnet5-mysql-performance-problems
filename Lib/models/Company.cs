using System;

namespace AccountSys3.Lib.Models {

    public class Company {
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public string CsvSeperator { get; set; }
        public long CsvCountryCode { get; set; }
        public string CsvDateFormat { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }

        public string CompanyRegistrationNo { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public DateTimeOffset Updated { get; set; }
        public long IsUkNumberFormat { get; set; }
    }

}
