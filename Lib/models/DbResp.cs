namespace AccountSys3.Lib.Models {

    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DbResp<T> {

        public IEnumerable<T> Data { get; set; }
        public int RowsEffected { get; set; }
        public string Error { get; set; }
        public long ExeTimeMS { get; set; }
        public string ClassName { get; }
        public string MethodName { get; }

        public DbResp([CallerFilePath] string callingClass = "", [CallerMemberName] string callingMethod = "") {

            // get className and method
            var pathArr = callingClass.Split('/');
            this.ClassName = pathArr[pathArr.Length - 1];
            this.MethodName = callingMethod;
        }

        public override string ToString() {
            return string.Format("class={0} method={1} rows={2} exe={3}ms err={4} ", ClassName, MethodName, RowsEffected, ExeTimeMS, Error);
        }

    }
}