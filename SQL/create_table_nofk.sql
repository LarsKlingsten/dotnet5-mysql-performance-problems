-- accounts3.AccountEntryPerformanceNoFK definition
-- no foreign keys

CREATE TABLE `AccountEntryPerformanceNoFK` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `CompanyID` int NOT NULL,
  `CustomID` int NOT NULL,
  `Debit` int DEFAULT NULL,
  `Credit` int DEFAULT NULL,
  `EntryDate_OLD` datetime DEFAULT NULL,
  `Amount` decimal(13,2) DEFAULT NULL,
  `Vat` decimal(6,3) DEFAULT NULL,
  `EntryText` varchar(50) DEFAULT NULL,
  `IsManualEntry` bit(1) NOT NULL,
  `HasInvoice` bit(1) NOT NULL,
  `ClientID` int DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `isDeleted` bit(1) NOT NULL,
  `RuleNumber` int NOT NULL,
  `RuleType` int NOT NULL,
  `AmountCur` decimal(13,2) DEFAULT NULL,
  `ExchangeRate` decimal(12,6) DEFAULT NULL,
  `EntryDate` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `AccountEntry_CompanyID_IDX` (`CompanyID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22755 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;