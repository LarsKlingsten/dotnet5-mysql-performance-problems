using Xunit;
using AccountSys3.Lib.DataAccess;
using AccountSys3.Lib.Service;
using AccountSys3.Lib.Models;
using AccountSys3.Lib.Interfaces;

namespace UnitTests {

    public class DataServiceTests {
        [Fact]
        public void CreateModel() {

            var expect = new DbResp<string>();
            expect.Error = "not implemented";

            IDataAccess dataAccess = new MsSqlData("dummyConnectionString");
            var svcData = new DataService(dataAccess);

            var result = svcData.TestUnitTest();
            Assert.Equal(expect.Error, result.Error);
        }

    }

}
