﻿namespace AccountSys3.App {

    using System;

    using AccountSys3.Lib.Interfaces;
    using AccountSys3.Lib.DataAccess;
    using AccountSys3.Lib.Service;
    using AccountSys3.Lib.Enums;

    class Program {

        static void Main(string[] args) {

            Console.WriteLine("MYSQL Performance! (starts)\n");

            // var msSqlClient = GetDataService(DatabaseTypes.MsSQL_SqlClient);
            var mySqlData = GetDataService(DatabaseTypes.MySQL_Data);
            var mySqlDataNoDapper = GetDataService(DatabaseTypes.MySQL_Data_NoDapper);
            var mySqlMyConnectorNoDapper = GetDataService(DatabaseTypes.MySql_MyConnector_NoDapper);
            var mySqlMyConnector = GetDataService(DatabaseTypes.MySql_MyConnector);

            var company1 = 1030;
            var company2 = 1037;


            // Warm up
            Console.WriteLine("Warmup");

            // msSqlClient.GetAccountEntries(company1);
            mySqlData.GetAccountEntries(company1);
            mySqlDataNoDapper.GetAccountEntries(company1);
            mySqlMyConnectorNoDapper.GetAccountEntries(company1);
            mySqlMyConnector.GetAccountEntries(company1);
            //  msSqlClient.GetAccountEntries(company2);
            mySqlData.GetAccountEntries(company2);
            mySqlDataNoDapper.GetAccountEntries(company2);
            mySqlMyConnectorNoDapper.GetAccountEntries(company2);
            mySqlMyConnector.GetAccountEntries(company2);


            Console.WriteLine();
            Console.WriteLine("Exe");
            // // Exe
            // msSqlClient.GetAccountEntries(company1);
            // msSqlClient.GetAccountEntries(company2);

            mySqlData.GetAccountEntries(company1);
            mySqlData.GetAccountEntries(company2);

            mySqlDataNoDapper.GetAccountEntries(company1);
            mySqlDataNoDapper.GetAccountEntries(company2);

            mySqlMyConnectorNoDapper.GetAccountEntries(company1);
            mySqlMyConnectorNoDapper.GetAccountEntries(company2);

            mySqlMyConnector.GetAccountEntries(company1);
            mySqlMyConnector.GetAccountEntries(company2);

            // we run it again
            mySqlData.GetAccountEntries(company1);
            mySqlData.GetAccountEntries(company2);





        }

        private static DataService GetDataService(DatabaseTypes databaseType) {
            SqlConnectionStringService connection = new SqlConnectionStringService(databaseType);

            (var connStr, var error) = connection.GetConnectionString();
            if (error != null) {
                Console.WriteLine("{0}", error);
                return null;
            }

            IDataAccess dataAccess;
            if (databaseType == DatabaseTypes.MySQL_Data) {
                dataAccess = new MySqlData(connStr);
            } else if (databaseType == DatabaseTypes.MsSQL_SqlClient) {
                dataAccess = new MsSqlData(connStr);
            } else if (databaseType == DatabaseTypes.MySQL_Data_NoDapper) {
                dataAccess = new MySqlDataNoDapper(connStr);
            } else if (databaseType == DatabaseTypes.MySql_MyConnector_NoDapper) {
                dataAccess = new MySqlMyConnectorNoDapper(connStr);
            } else if (databaseType == DatabaseTypes.MySql_MyConnector) {
                dataAccess = new MySqlMyConnector(connStr);
            } else {
                throw new Exception("No Implementation for database type for {databaseType}.");
            }

            var svcData = new DataService(dataAccess);
            return svcData;

        }



    }


}
